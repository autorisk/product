var express = require('express');
var app = express();
var cors= require('cors');
var mongoose = require('mongoose');
var statisticHelper = require('./StatisticHelper');
var insightOutsideStdDev = require('./InsightOutsideStdDev');
var insightOutlier = require('./InsightOutlier');
var insightCorrelation = require('./InsightCorrelation');
//var insightMl = require('./InsightMLRegression');
var db = require('./read_file');

app.use(cors(
    {
        origin: 'http://localhost:4200'
      }
));

app.listen(3000, function() {
    console.log("Server is running at 3000 port!");   
});



app.get('/api/Insight', function(req, res) {

    //var sd = new statisticHelper;
    //var ret = statisticHelper.GetStandardDeviation([1, 3, 5, 3, 2, 1, 10]);

   //res.send('average = ' + ret.average.toString() + '     ' + 'standard dev = ' + ret.standardDeviation.toString());

    db.list_risk_mongodb()
        .then(function(riskdata) {

        const groupings = [
            'department',
            'location_id',
            'grup_team',
            'businness_contact',
            'user_entered',
            'issue_count',
            'org_id',
            'biz_container',
            "procedure_count",
            "count_issue",
            "control_count"
        ];

        const scoreVariables = [
            'inherent_score',
            'residual_score',
            'origin_score',
            'expected_result'
        ];

        const statParams = [];
        let output = [];

        groupings.forEach((g, i) => {
            //valueFieldName
            
            scoreVariables.forEach(sv => {
                console.log('Grouping: ' + g);
                console.log('scoreVariable: ' + sv);
                const statParam = {
                    keyFieldName: g,
                    valueFieldName: sv,
                    data: [],
                }

                statParam.data = riskdata.map(rd => {
                    return {
                        key: rd[g],
                        value: rd[sv],
                    }
                });

               // console.log(statParam.data);
                //process.exit(0);

                statParam.insight1 = insightOutsideStdDev.GetInsights(statParam);
                statParam.insight2 = insightOutlier.GetInsights(statParam);
                statParams.push(statParam);
                console.log(i);
            });

            statParams.forEach(sp => {
                sp.data = [];
                output = output.concat(sp.insight1).concat(sp.insight2);
            });
        });

        scoreVariables.forEach(sv => {

            const statParams = {
                target: {
                    data: riskdata.map(rd => { return rd[sv] }),
                    name: sv,
                },
                series: []
            };
            
            groupings.forEach((g, i) => {

                const seriesData = {
                    data: riskdata.map(rd => { return rd[g] }),
                    name: g
                }
                
                statParams.series.push(seriesData);

            });

            let insights = insightCorrelation.GetInsights(statParams);
            output = output.concat(insights);
  
        });

        
        res.json(output);
    });

});


app.get('/api/RiskData', function(req, res) {

    db.list_risk_mongodb()
        .then(function(riskdata) {
            res.json(riskdata);
        });

});



app.get('/api/test', function(req, res) {

   var arr = {
    keyFieldName: "user",
    valueFieldName: "risk score",
    data: [
       { key:"user 1", value:15 },
       { key:"user 2", value:1 },
       { key:"user 2", value:50 },
       { key:"user 2", value:58 },
       { key:"user 1", value:57 },
       { key:"user 1", value:6 },
       { key:"user 2", value:53 },
       { key:"user 1", value:54 },
       { key:"user 1", value:53 },
       { key:"user 1", value:54 },
       { key:"user 3", value:52 },
       { key:"user 1", value:51 },
       { key:"user 1", value:51 },
       { key:"user 3", value:100 },
       { key:"user 1", value:55 },
       { key:"user 3", value:54 },
       { key:"user 4", value:500 }
    ]
   };
   
    var corrData = {
        target: {
            data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            name: "risk score",
            },
        series: [
            { data: [1, 3, 3, 4, 5, 7, 7, 8, 9, 10],
              name: "field1" },
            { data: [10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
              name: "field2" },
            { data: [11, 2, 33, 44, 51, 6, 17, 18, 19, 10],
              name: "field3" },
            { data: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
              name: "field4" },
            { data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
              name: "field5" }
        ]
       };


       var ret3 = insightCorrelation.GetInsights(corrData);
    
       res.json(ret3);

});