var statisticHelper = require('./StatisticHelper');

module.exports.GetInsights = function GetInsights(data)
{
    let allScores = new Array();
    let allKeys = new Array();
    data.data.forEach(row => {
        allScores.push(row.value);
        if(allKeys.indexOf(row.key) == -1)
        {
            allKeys.push(row.key);
        }
    });

    let stdDev = statisticHelper.GetStandardDeviation(allScores);
    let expectedMin = stdDev.average - 2 * stdDev.standardDeviation;
    let expectedMax = stdDev.average + 2 * stdDev.standardDeviation;
    let keyFieldName = data.keyFieldName;
    let valueFieldName = data.valueFieldName;
    let insights = new Array();

    allKeys.forEach(k => {
        let scores = new Array();
        data.data
            .filter(d => d.key === k)
            .forEach(score => {
                
                scores.push(score.value);
            });

            let avg = statisticHelper.Average(scores);
        
            if(avg < expectedMin) {
                let insight = {
                    expectedMin: expectedMin,
                    expectedMax: expectedMax,
                    insightType: "OutsideStdDev",
                    keyValue: k,
                    insightMessage: "Value of " + valueFieldName + " for " + keyFieldName + " " + k + " is consistently smaller than 2 standard deviations from the norm."
                };
                insights.push(insight);
            }
            else if(avg > expectedMax) {
                let insight = {
                    expectedMin: expectedMin,
                    expectedMax: expectedMax,
                    insightType: "OutsideStdDev",
                    keyValue: k,
                    insightMessage: "Value of " + valueFieldName + " for " + keyFieldName + " " + k + " is consistently greater than 2 standard deviations from the norm."
                };
                insights.push(insight);
            }
    });

    return insights;
}
