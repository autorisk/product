import { PagerService } from './services/pager.service';
import { ResultTableComponent } from './components/result-table/result-table.component';
import { InsightsService } from './services/insights.service';
import { ResultsService } from './services/results.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import { InsightsComponent } from './components/insights/insights.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

@NgModule({
  declarations: [
    AppComponent,
    ResultTableComponent,
    NavbarComponent,
    InsightsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AngularFontAwesomeModule
  ],
  providers: [ResultsService, InsightsService, PagerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
