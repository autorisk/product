import { InsightType } from './enums/insight-type.enum';

export class Insights {

    expectedMin: number;
    expectedMax: number;
    insightType: InsightType;
    keyValue: string;
    insightMessage: string;
}

