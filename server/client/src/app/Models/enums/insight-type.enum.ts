export enum InsightType {
    OutsideStdDev = 'OutsideStdDev',
    Correlation= 'Correlation',
    Outlier= 'Outlier'
}
