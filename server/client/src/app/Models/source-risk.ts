export class SourceRisk {
    audit_is: number;
    name: string;
    risk_id: number;
    date_time: Date;
    inherent_score: number;
    residual_score: number;
    department: string;
    location_id: string;
    grup_team: number;
    control_count: number;
    businness_contact: string;
    user_entered: string;
    issue_count: number;
    risk_description: string;
    biz_container: number;
    origin_score: number;
    org_id: string;
    expected_result: number;
}
