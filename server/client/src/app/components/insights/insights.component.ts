import { InsightsService } from './../../services/insights.service';
import { Component, OnInit } from '@angular/core';
import { Insights } from '../../Models/insights';
import { ArrayType } from '@angular/compiler/src/output/output_ast';
import { InsightType } from '../../Models/enums/insight-type.enum';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-insights',
  templateUrl: './insights.component.html',
  styleUrls: ['./insights.component.css']
})
export class InsightsComponent implements OnInit {

  Data: Insights[];
  Insights: InsightType[];
  HasRetrievedInsights = false;

  isLoading = false;
  InsightEngine: InsightsService;

  constructor(service: InsightsService) {
    this.Data = new Array<Insights>();
    this.InsightEngine = service;
  }

  requestInsights() {
    this.isLoading = true;
    this.InsightEngine.GetRequestedInsights(this.Insights)
      .subscribe(
        data => { this.Data = data;
          this.HasRetrievedInsights = true;
          this.isLoading = false;
        },
        error => console.log(error));

  
  }
  getIsLoading() { return this.isLoading; }
  ngOnInit() {
  }

}
