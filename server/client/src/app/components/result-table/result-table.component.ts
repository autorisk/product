import { SourceRisk } from './../../Models/source-risk';
import { ResultsService } from './../../services/results.service';
import { Component, OnInit } from '@angular/core';
import { PagerService } from '../../services/pager.service';

@Component({
  selector: 'app-result-table',
  templateUrl: './result-table.component.html',
  styleUrls: ['./result-table.component.css']
})
export class ResultTableComponent implements OnInit {

  title = 'Risk Results';
  Data: SourceRisk[];

  // array of all items to be paged
  private allItems: any[];

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];


  constructor(private service: ResultsService, private pagerService: PagerService) {

  }

  ngOnInit() {
    this.service.getRealData().subscribe(data => {
      // set items to json response
      this.allItems = data;
      // initialize to page 1
      this.setPage(1);
    }
    );
  }


  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

    // get pager object from service
    this.pager = this.pagerService.getPager(this.allItems.length, page);

    // get current page of items
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }
}
