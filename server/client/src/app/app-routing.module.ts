import { InsightsComponent } from './components/insights/insights.component';
import { ResultTableComponent } from './components/result-table/result-table.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path : 'data', component: ResultTableComponent },
  { path : 'insights', component: InsightsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
