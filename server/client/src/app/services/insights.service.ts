import { Insights } from './../Models/insights';
import { Injectable } from '@angular/core';
import { InsightType } from '../Models/enums/insight-type.enum';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class InsightsService {
  httpService: HttpClient;
  constructor(private http: HttpClient) { // DevSkim: ignore DS137138 until 2018-06-01 

  }


  GetRequestedInsights(requestedTypes: InsightType[]) {
    const insights = new Array<Insights>();
    // Make Request to Server for data here
    const url = "api/insight";
    let insightsFromServer = this.http.get<Array<Insights>>(url);
    return insightsFromServer;

  }
  
}
