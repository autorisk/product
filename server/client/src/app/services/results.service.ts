import { SourceRisk } from './../Models/source-risk';
import { Injectable } from '@angular/core';
import * as faker from 'faker';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class ResultsService {

  constructor(private http: HttpClient) { }

  getMockData() {
    const result: SourceRisk[] = new Array<SourceRisk>();
    for (let index = 0; index < 15; index++) {

      const item = new SourceRisk();
      item.audit_is = faker.random.number(100);
      item.name = faker.random.arrayElement(['Insufficient investment in training',
        'Over-reliance on one or a few key employees',
        'Inability to recruit / retain staff',
        'Failure to develop appropriate skills',
        'Loss of key employees',
        'Not offering attractive (enough) remuneration packages',
        'Ineffective health and safety management',
        'Lack of effective succession planning',
        'New or inexperienced management',
        'Not offering attractive career paths',
        'Employee / director fraud',
        'Lack of training',
        'Unexpected / unbudgeted cost increases',
        'Failure to achieve margins',
        'Failure to manage debt to equity ratio',
        'Ineffective asset management',
        'Ineffective assets performance measurement',
        'Accounting adjustments',
        'Insufficient capital availability',
        'Inadequate systems to calculate tand monitor margins',
        'Failure to collect all income due',
        'Adverse impact of exchange rate fluctuations',
        'Failure to meet earnings targets',
        'Ineffective buying / spending on or using of financing methods',
        'Alternative funding sources identified are not appropriate',
        'Failure to achieve profitability goals',
        'Operating Margin Risks',
        'Assets not properly secured and insured',
        'Strategy is not implemented',
        'Failure to have and execute R&D plans',
        'Risk management practices do not exist',
        'Regulatory / legal limitations',
        'No change management process',
        'Systems are not integrated',
        'Strategy is implemented improperly',
        'Brand / reputation damage by company environmental failure',
        'Failure to identify market opportunities',
        'Increase of competitors in the marketplace',
        'Falling market conditions',
        'Criminal act causing environmental damage',
        'Environmentally hazardous / sensitive materials not managed effectively',
        'Increased raw materials prices due to market shortages caused by environmental incident',
        'Ineffective recycling strategies',
        'Competitive advantage period is not known',
        'Supply of raw materials affected by environmental failure',
        'Aggressive competitor activity',
        'Uncompetitive products and services',
        'Lack information regarding sales prices and profit margins',
        'Failure to meet demand',
        'Loss of business through damage to reputation of company',
        'New opportunities to create or extend competitive advantage periods are not considered',
        'Sales personnel unaware of the period of advantage and do not know their products and services',
        'Loss of business through unattractive product / service offerings',
        'Loss to competitors through competitor efforts',
        'Competitive Advantage Period Risks',
        'Poor value for money',
        'Diversification of products and services at the expense of core business',
        'Aggressive competitor activity',
        'Natural disaster',
        'Changing customer expectations',
        'Failure to achieve global expansion',
        'Product development not matching external needs',
        'Regulatory / legal constraints',
        'Risk in volatility in emerging markets',
        'Poor quality / reliability record',
        'Competitive period is not measured and monitored',
        'Adverse market conditions',
        'Community involvement objectives are not clearly articulated',
        'Imposition of import/export taxes in home or overseas market',
        'Foreign government support for home market',
        'Production shutdown by regulators for law / regulation infringement',
        'Government shuts down overseas / export market for political reasons',
        'Charitable contributions by foreign operations are not well controlled',
        'Implication of new labor laws',
        'Failure to recognize community awareness in foreign countries resulting in adverse publicity',
        'Ineffective public relations causing adverse publicity',
        'Time off for community involvement is no adequately controlled',
        'Industrial sabotage',
        'Legal changes affecting the availability of raw materials',
        'Failure to comply with government ethical policies / regulations',
        'Community awareness programs are ineffective',
        'Failure to comply with government sponsored ethical policies / regulations',
        'Charitable contributions are not adequately controlled',
        'Increased tax burden on products / profits',
        'Fines for failure to comply with laws / regulations',
        'Customer needs change and organization is not aware',
        'Changing customer expectations',
        'High service costs will reduce profitability over the product life cycle',
        'Loss of customers through poor customer after sales service',
        'Customer service is not sufficient',
        'Customer fraud',
        'Customer is not satisfied',
        'Failure to focus on and manage key customers effectively',
        'Failure to optimize customer access to the company by electronic means (E-commerce)',
        'Loss of customers through poor customer management',
        'Unauthorized changes will be made to programs, master files, and reference data',
        'Data will not be processed accurately or properly',
        'Systems will not be properly implemented',
        'Systems will not be designed according to user needs',
        'Fraud']);
      item.risk_id = index + 1;
      item.date_time = faker.date.past(2);
      item.inherent_score = faker.random.number(10);
      item.residual_score = faker.random.number(10);
      item.department = faker.commerce.department();
      item.location_id = faker.address.city();
      item.grup_team = faker.random.number(25);
      item.control_count = faker.random.number(75);
      item.businness_contact = faker.name.findName();
      item.user_entered = faker.name.findName();
      item.issue_count = faker.random.number(10);
      item.risk_description = faker.lorem.sentence(5);
      //  item.org_id= faker.random.uuid();
      item.biz_container = faker.random.number(100);
      item.origin_score = faker.random.number(5);
      item.expected_result = faker.random.number(5);
      result.push(item);
    }
    return result;
  }

  getRealData() {
    //  const insights = new Array<SourceRisk>();
    // Make Request to Server for data here
    const url = "api/RiskData";
    let riskFromServer = this.http.get<Array<SourceRisk>>(url);
    return riskFromServer;
  }

}
