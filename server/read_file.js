
const bodyParser = require('body-parser');
//loading the path module
const path = require('path');
//loading the OS module
const os = require('os');
//loading the filesystem module
var fs = require('fs');
//-----MongoDB connection-----
var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;


const express = require('express');
const app = express();

//enable parsing a JSON objects from the request
app.use(express.json());

//--------read a file in a file-----------
function list_risk_file(file_name) {
    var filePath = path.join(__dirname, file_name);
    //var obj = JSON.parse(fs.readFileSync(filePath, 'utf8'));
    var list_risks = new Array();

    fs.readFile(filePath, 'utf8', function (err, data,callback) {
        if (err) throw err;
        let risks = JSON.parse(data);
        risks.forEach(risk => {
            list_risks.push(risk);
            //console.log(risk.department);
        });
        return callback;
    });
    return list_risks;
}

var a = list_risk_file('data.json');
console.log(a);

module.exports.list_risk_file;

//using mongodb locally -----read db---
module.exports.list_risk_mongodb = function list_risk_mongodb(purl) {
    var url = 'mongodb://localhost:27017';
    var list_departments = new Array();
    if (purl != undefined) url = purl;

    return MongoClient.connect(url)
        .then(function (connectResult) {
            var dbo = connectResult.db("autorisk");

            return dbo.collection("risks").find({}).toArray();
            /*.then(function (result) {
                for (var i = 0; i < result.length; i++) {
                    list_departments.push(result[i].department);
                    //console.log(list_departments);
                    //console.log(result[i].department);
                }
                return list_departments;
            });*/
        })
        .then(function (riskResults) {
            //console.log(riskResults);
            return riskResults;
        });
};





