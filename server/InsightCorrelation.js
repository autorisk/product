var statisticHelper = require('./StatisticHelper');

module.exports.GetInsights = function GetInsights(data)
{
    let insights = new Array();

    data.series.forEach(s =>
    {
        let corr = statisticHelper.getPearsonCorrelation(data.target.data, s.data);
        if(corr > .5)
        {
            let insight = {
                //expectedMin: expectedMin,
                //expectedMax: expectedMax,
                insightType: "Correlation",
                //keyValue: series.name,
                correlationValue: corr,
                insightMessage: "There is a strong positive correlation between " + data.target.name + " and " + s.name + "."
            };
            insights.push(insight);
        }
        else if(corr < -.5)
        {
            let insight = {
                //expectedMin: expectedMin,
                //expectedMax: expectedMax,
                insightType: "Correlation",
                //keyValue: ,
                correlationValue: corr,
                insightMessage: "There is a strong negative correlation between " + data.target.name + " and " + s.name + "."
            };
            insights.push(insight);
        }
    });

    return insights;
}