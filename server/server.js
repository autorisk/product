var express = require('express');
var app = express();
var mongoose = require('mongoose');

app.get('/', function(req, res) {
 res.send('Hello Express');
});
 
app.listen(3000, function() {
 console.log("Server is running at 3000 port!");
});

app.get('/Create', function(req, res) {
    
    mongoose.connect('mongodb://admin:password@ds263989.mlab.com:63989/autorisk');

    jrsSchema = new mongoose.Schema({ name: String, id: Number},
        { collection : 'jrs_test' });
    //jrsSchema.save();    
    
    var myModel = mongoose.model('Entry', jrsSchema);
    var entry = new myModel();
    entry.name = "testing";
    entry.id = 400;
    entry.save(function (err) {
        if (err) return handleError(err);
        res.send("it worked");
      });

});

app.get('/Query', function(req, res) {
    
    mongoose.connect('mongodb://admin:password@ds263989.mlab.com:63989/autorisk');

    jrsSchema = new mongoose.Schema({ name: String, id: Number}, 
       { collection : 'jrs_test' });

    var entry = mongoose.model('Entry', jrsSchema);

    entry.findOne({ 'name': 'testing'}, 'name id',  function (err, person) {
      if (err) return handleError(err);
      // Prints "Space Ghost is a talk show host".
      res.send(entry.name + entry.id);
    });

});
