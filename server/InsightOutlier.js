var statisticHelper = require('./StatisticHelper');

module.exports.GetInsights = function GetInsights(data)
{
    var allScores = new Array();
    data.data.forEach(row => 
        {
            allScores.push(row.value);
        });

    var range = statisticHelper.GetOutlierRange(allScores);

    var insights = new Array();
    data.data.forEach(row => 
        {
            if(row.value < range.lowerBound)
            {
                var insight = {
                    expectedMin: range.lowerBound,
                    expectedMax: range.upperBound,
                    insightType: "Outlier",
                    keyValue: row.key + "/" + row.value,
                    insightMessage: "Value of " + data.valueFieldName + " for " + data.keyFieldName + " " + row.key + " is an outlier that is significantly smaller than the norm."
                };
                insights.push(insight);            
            }
            else if(row.value > range.upperBound)
            {
                var insight = {
                    expectedMin: range.lowerBound,
                    expectedMax: range.upperBound,
                    insightType: "Outlier",
                    keyValue: row.key + "/" + row.value,
                    insightMessage: "Value of " + data.valueFieldName + " for " + data.keyFieldName + " " + row.key + " is an outlier that is significantly larger than the norm."
                };
                insights.push(insight);            
            }

        });

    return insights;
}
