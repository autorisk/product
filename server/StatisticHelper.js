
module.exports.GetStandardDeviation = function GetStandardDeviation(values)
{
  var avg = average(values);

  var diffs = values.map(function(value){
      var diff = value - avg;
      return diff;
  });

  var squareDiffs = values.map(function(value){
      var diff = value - avg;
      var sqr = diff * diff;
      return sqr;
  });

  var avgSquareDiff = average(squareDiffs);
  var stdDev = Math.sqrt(avgSquareDiff);

  var ret = {
      average: avg,
      standardDeviation: stdDev
  };

  return ret;
}

module.exports.GetOutlierRange = function GetOutlierRange(values)
{
  values = values.sort(function(a, b){return a - b});
  //var med = median(values);
  
  var half_length = Math.ceil(values.length / 2);
  var topValues = values.splice(0,half_length);
  //var bottomValues = values.splice(half_length, values.length - half_length - 1);

  var q1 = median(topValues);
  var q3 = median(values);
  var range = q3 - q1;

  var ret = {
    lowerBound: q1 - 3 * range,
    upperBound: q3 + 3 * range
  };

  return ret;
}

module.exports.Average = function avgImpl(data)
{
    return average(data);
}

module.exports.Median = function medianImpl(data)
{
    return median(data);
}

function average(data)
{
    var sum = data.reduce(function(sum, value){
      return sum + value;
    }, 0);
  
    var avg = sum / data.length;
    return avg;  
}

function median(numbers) {
  var median = 0, numsLen = numbers.length;
  numbers.sort(function(a, b){return a - b});

  if (
      numsLen % 2 === 0 // is even
  ) {
      // average of two middle numbers
      median = (numbers[numsLen / 2 - 1] + numbers[numsLen / 2]) / 2;
  } else { // is odd
      // middle number only
      median = numbers[(numsLen - 1) / 2];
  }

  return median;
}

/*
 *  Source: http://stevegardner.net/2012/06/11/javascript-code-to-calculate-the-pearson-correlation-coefficient/
 */
module.exports.getPearsonCorrelation = function GetPearsonCorrelation(x, y) {
  var shortestArrayLength = 0;
   
  if(x.length == y.length) {
      shortestArrayLength = x.length;
  } else if(x.length > y.length) {
      shortestArrayLength = y.length;
      console.error('x has more items in it, the last ' + (x.length - shortestArrayLength) + ' item(s) will be ignored');
  } else {
      shortestArrayLength = x.length;
      console.error('y has more items in it, the last ' + (y.length - shortestArrayLength) + ' item(s) will be ignored');
  }

  var xy = [];
  var x2 = [];
  var y2 = [];

  for(var i=0; i<shortestArrayLength; i++) {
      xy.push(x[i] * y[i]);
      x2.push(x[i] * x[i]);
      y2.push(y[i] * y[i]);
  }

  var sum_x = 0;
  var sum_y = 0;
  var sum_xy = 0;
  var sum_x2 = 0;
  var sum_y2 = 0;

  for(var i=0; i< shortestArrayLength; i++) {
      sum_x += x[i];
      sum_y += y[i];
      sum_xy += xy[i];
      sum_x2 += x2[i];
      sum_y2 += y2[i];
  }

  var step1 = (shortestArrayLength * sum_xy) - (sum_x * sum_y);
  var step2 = (shortestArrayLength * sum_x2) - (sum_x * sum_x);
  var step3 = (shortestArrayLength * sum_y2) - (sum_y * sum_y);
  var step4 = Math.sqrt(step2 * step3);
  var answer = step1 / step4;

  return answer;
}